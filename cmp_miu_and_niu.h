#include <blitz/array.h>
//using namespace blitz;

typedef blitz::Array<double,1> V1D;
typedef blitz::Array<double,2> V2D;
typedef blitz::Array<double,3> V3D;

int cmp_miu_and_niu(const char *fname_p,
					const char *fname_h,
					const char *fname_w,
					double r1, double r2,
				   	double theta1, double theta2,
				   	double z1, double z2,
					size_t dim_z,
				    V3D &mius, V3D &nius);	

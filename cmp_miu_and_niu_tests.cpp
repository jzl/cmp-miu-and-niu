#include "cmp_miu_and_niu.h"
#include <iostream>

using namespace std;

int main()
{
const char* fname_p = "blitz_ps.dat";
const char* fname_h = "blitz_hs.dat";
const char* fname_w = "blitz_ws.dat";
double r1, r2, theta1, theta2, z1, z2;
size_t dimz = 11;
	
	V3D mius, nius;
	r1     = 1.3359,   r2 = 3.7571;
	theta1 = 0.0,  theta2 = 0.3927;
	z1     = 0.0,      z2 = 1.0;

	cmp_miu_and_niu(fname_p, fname_h, fname_w, r1, r2, theta1, theta2, z1, z2, dimz, mius, nius);

	cout << mius << endl;
}

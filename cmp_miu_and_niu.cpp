#include "cmp_miu_and_niu.h"
#include <blitz/array/expr.h>
#include <blitz/array/stencil-et.h>
#include <fstream>

BZ_USING_NAMESPACE(blitz);
using namespace blitz::tensor;

firstIndex ii;
secondIndex jj;
thirdIndex kk;


#define RDATA(fname, V) ifstream ifs##fname(fname);\
	if(ifs##fname.bad()) {cerr<<"Unable to open "<<fname<<endl;exit(1);}\
	ifs##fname>>V;\
	ifs##fname.close();\



#define Intgral_along_the_last_dim(V3R,V3, h) for(int i = 0; i < V3.shape()[0]; i++)\
										   for(int j = 0; j < V3.shape()[1]; j++){\
										     double intgral = 0.0;\
											 int k;\
										     for(k = 0; k < V3.shape()[2] - 1; k++){\
											    V3R(i,j,k) = intgral;\
											    intgral += (V3(i,j,k) + V3(i,j,k+1))*h/2;\
											    }\
										     V3R(i,j,k) = intgral;}

int cmp_miu_and_niu(const char *fname_p,
					const char *fname_h,
					const char *fname_w,
					double r1, double r2, double theta1, double theta2, double z1, double z2,
					size_t dim_z,
				    V3D &mius, V3D &nius)	
{
double omeg, B, U;
size_t dim_r, dim_theta;

V1D r, theta, z;
V2D p, h0, h, w, p_derive_theta, p_derive_r;
V3D eta, one_div_eta, z_div_eta, iode, izde;


/******************** initialize some customized data. ************************************/
	omeg   = 186.4;
	B      = 0.1198;
	U      = 56.85;
/* ************************************************************************************* */
	

/******************* read data from file *************************************************/
	RDATA(fname_p, p);
	RDATA(fname_h, h0);
	RDATA(fname_w, w);
/* ************************************************************************************* */


/* ****************** compute the r,theta,z vectors.  ********************************** */
	dim_theta   = p.shape()[0];
	dim_r       = p.shape()[1];

	r.resize(dim_r);
	double stepr = (r2 -r1) / (r.size() - 1);
	r            = r1 + stepr * ii; 

	theta.resize(dim_theta);
	double stept = (theta2 - theta1) / (theta.size() -1);
	theta        = theta1 + stept * ii;
	
	z.resize(dim_z);
	double stepz = (z2 - z1) / (z.size() - 1);
	z            = z1 + stepz * ii;
/* ************************************************************************************* */


/******************* compute h ***********************************************************/
	h.resize(h0.shape());
	h = h0 + w;
/* ************************************************************************************* */


/* ***************** compute p's derivation against theta and r************************* */
	p_derive_theta.resize(p.shape());
	p_derive_theta(Range(1, dim_theta - 2), Range::all()) = central12(p, 0) / (2*stept);
	p_derive_theta(Range(0,0), Range::all()) =\
				  forward11(p(Range(0,1), Range::all()), 0) / stept;
	p_derive_theta(Range(dim_theta-1,dim_theta-1), Range::all()) = \
				  backward11(p(Range(dim_theta-2,dim_theta-1), Range::all()), 0) / stept;

	p_derive_r.resize(p.shape());
	p_derive_r(Range::all(), Range(1, dim_r - 2)) = central12(p, 1) / (2*stepr);
	p_derive_r(Range::all(), Range(0,0)) =\
			   forward11(p(Range::all(), Range(0, 1)), 1) / stepr;
	p_derive_r(Range::all(), Range(dim_r -1, dim_r - 1)) = \
			   backward11(p(Range::all(), Range(dim_r - 2, dim_r - 1)), 1) / stepr;
/* ************************************************************************************* */




/* ************ compute eta one divide eta ,a divide eta, and there integral against z** */
	eta.resize(dim_theta, dim_r, dim_z);
	one_div_eta.resize(eta.shape());
	z_div_eta.resize(eta.shape());

	eta         = 1.0;     //
	one_div_eta = 1.0 / eta;
	z_div_eta   = z(kk) / eta(ii,jj,kk);
/* ************************************************************************************* */


/* ************* compute 1/eta and z/eta 's integral against z************************** */
	iode.resize(eta.shape());
	izde.resize(eta.shape());

	Intgral_along_the_last_dim(iode, one_div_eta, stepz);
	Intgral_along_the_last_dim(izde, z_div_eta, stepz);


	// convert iode[1] and izde[1] to 3d
	V2D iode1(p.shape());
	iode1 = iode(Range::all(), Range::all(),dim_z - 1);

	V2D izde1(p.shape());
	izde1 = izde(Range::all(), Range::all(),dim_z - 1);
/* ************************************************************************************* */


/* *************** compute the final result ******************************************** */
	mius.resize(eta.shape());
	nius.resize(eta.shape());


	mius = (omeg*B/U)*r(jj)*(1.0 - iode(ii,jj,kk) / iode1(ii,jj)) +\ 
		   (h(ii,jj) * h(ii,jj))*\
		   						(izde(ii,jj,kk) - (iode(ii,jj,kk) * izde1(ii,jj) / iode1(ii,jj))) *\
								p_derive_theta(ii,jj) / r(jj);


	nius = h(ii,jj) * h(ii,jj)*\
		   (izde(ii,jj,kk) - (iode(ii,jj,kk) * izde1(ii,jj) / iode1(ii,jj))) *\
		   p_derive_r(ii,jj);
/* ************************************************************************************* */
	return 0;
}


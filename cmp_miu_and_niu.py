#!/usr/bin/env python
import numpy as np
from scipy import integrate
from IPython import embed
import time

start = time.time()


def rdata(filename, _type = float):
    with open(filename,'r') as f:
        return np.array([map(_type, l.split()) for l in f])


def arr2str(arr):
    return '\n'.join(map(lambda l:' '.join(map(str,l)), arr))


if __name__ == '__main__':
    r1 = 1.3359
    r2 = 3.7571
    alpha1 = 0
    alpha2 = 0.3927
    omg = 186.4
    B = 0.1198
    U = 56.85

    eta = np.ones((11,101,101), dtype=np.float)

    zs  = np.linspace(0,1,11)

    eta_z = zs.reshape(11,1,1) * eta

    itg_eta = np.empty_like(eta)
    for i in range(11):
        itg_eta[i,:,:] = integrate.simps(eta[0:i+1, :,:],zs[:i+1], axis = 0)

    itg_eta_z = np.empty_like(eta_z)
    for i in range(11):
        itg_eta_z[i,:,:] = integrate.simps(eta_z[0:i+1, :,:],zs[:i+1], axis = 0)



    rs = np.linspace(r1,r2,101)
    thetas = np.linspace(alpha1, alpha2, 101)


    h0s = rdata('hs.dat')
    ps = rdata('ps.dat')
    ws = rdata('ws.dat')

    hs = h0s + ws

    dps_t, dps_r = np.gradient(ps,thetas[1]-thetas[0],rs[1] - rs[0])

    mius = np.empty_like(eta)
    weis = np.empty_like(eta)

    for i in range(mius.shape[0]):
        mius[i] = (omg*B/U)*rs*(1 - itg_eta[i]/itg_eta[-1]) +\
               (hs**2)*(itg_eta_z[i] - itg_eta_z[-1]*itg_eta[i]/itg_eta[-1])*\
               (dps_t/rs)

        weis[i] = (hs**2) * \
                  (itg_eta_z[i] - itg_eta_z[-1]*itg_eta[i]/itg_eta[-1])*\
                  dps_r
              
    out_miu = open("mius.dat","w")
    out_wei = open("weis.dat","w")
    for i in range(mius.shape[0]):
        out_miu.write("z=%f\n"%zs[i])
        out_wei.write("z=%f\n"%zs[i])
        out_miu.write(arr2str(mius[i])+'\n')
        out_wei.write(arr2str(weis[i])+'\n')
    out_miu.close()
    out_wei.close()

    print "time used:%f"%(time.time()-start)
    embed()
    pass

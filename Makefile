CXX = g++
CXXFLAGS = -ftemplate-depth-30 -g -DBZ_DEBUG
LDFLAGS = 
LIBS = -lblitz -lm
SRC=$(wildcard *.cpp)
OBJS=$(patsubst %.cpp,%.o,$(SRC))
PROGRAM=cmp_miu_and_niu_test


all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CXX) $(LDFLAGS) $(OBJS) -o $@ $(LIBS)

$(OBJS): $(SRC)
	$(CXX) $(CXXFLAGS) -c $(SRC)

clean:
	-rm *.o
	-rm $(TARGETS)
